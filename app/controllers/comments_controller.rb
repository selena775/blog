class CommentsController < ApplicationController
  before_action :find_post
  before_action :set_comment, only: [:show, :edit, :update, :destroy]


  # GET /posts/1/comments
  # GET /posts/1/comments.json
  def index
     @comments = @post.comments.load
  end

  # GET /posts/1/comments/1
  # GET /posts/1/comments/1.json
  def show
  end

  # GET /comments/new
  def new
    @comment = Comment.new
  end

  # GET /posts/1/comments/1/edit
  def edit
  end

  # POST /posts/1/comments
  # POST /posts/1/comments.json
  def create
    @comment = @post.comments.create(comment_params)

    respond_to do |format|
      if @comment.save
        format.html { redirect_to @post, notice: 'Comment was successfully created.' }
        format.json { render action: 'show', status: :created, location: @comment }
        format.js #create.js.erb
      else
        format.html { render action: 'new' }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1/comments/1
  # PATCH/PUT /posts/1/comments/1.json
  def update
    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_to flash[:from], notice: 'Comment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1/comments/1
  # DELETE /posts/1/comments/1.json
  def destroy
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to @post, notice: 'Comment was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    def find_post
        @post = Post.find(params[:post_id])
      end

    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = @post.comments.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comment_params
      params.require(:comment).permit(:post_id, :body)
    end
end
